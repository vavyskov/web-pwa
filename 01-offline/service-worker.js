/**
 * PWA: Service Worker je JavaScript, který je spuštěn (běží prohlížeči) i v době, kdy uživatel na webu zrovna není.
 * (off-line režim, rozesílání push notifikací atd.)
 */

// Uložení offline dat
var staticCacheName = "01-offline-cache-store";

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(staticCacheName).then((cache) => cache.addAll([
      '/01-offline/',
      '/01-offline/index.html',
      '/01-offline/js/pwa.js',
      '/01-offline/js/script.js',
      '/01-offline/css/style.css',
      '/01-offline/assets/icons/icon-48x48.ico',
      '/01-offline/assets/icons/icon-192x192-background-maskable.png',
      '/01-offline/assets/icons/icon.svg',
      '/01-offline/assets/icons/icon-maskable.svg',
    ])),
  );
});

self.addEventListener('fetch', (event) => {
  console.log(event.request.url);
  event.respondWith(
    caches.match(event.request).then((response) => response || fetch(event.request)),
  );
});
