/**
 * Register the Service Worker to control making site work offline
 */
if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/01-offline/service-worker.js')
    .then(() => { console.log('Service Worker successful Registered'); });
}

/* window.addEventListener('load', async () => {
    if ('serviceWorker' in navigator) {
        try {
            await navigator
                .serviceWorker
                .register('/01-offline/service-worker.js')
                then((registration) => {
                  console.log('Service Worker successful Registered with scope: ', registration.scope);
                });
        }
        catch (error) {
            console.log('Service Worker registration failed: ', error);
        }
    }
}); */



/**
 * Network status indicator
 */
var networkStatus = document.getElementById("network-status");

function updateNetworkStatus(event) {
  if (navigator.onLine) {
    document.body.classList.remove("offline");
    networkStatus.innerHTML = null;
  } else {
    document.body.classList.add("offline");
    networkStatus.innerHTML = 'Jste v režimu <strong>Offline</strong>.';
  }

}

window.addEventListener('online',  updateNetworkStatus);
window.addEventListener('offline', updateNetworkStatus);

// Page reload detection (F5, Ctrl+R)
if (performance.getEntriesByType("navigation")[0].type == 'reload') {
  updateNetworkStatus();
}



/**
 * Code to handle install prompt on desktop
 */
let deferredPrompt;
const addBtn = document.querySelector('.add-button');
addBtn.style.display = 'none';

window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Update UI to notify the user they can add to home screen
  addBtn.style.display = 'block';

  addBtn.addEventListener('click', () => {
    // hide our user interface that shows our A2HS button
    addBtn.style.display = 'none';
    // Show the prompt
    deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
    deferredPrompt.userChoice.then((choiceResult) => {
      if (choiceResult.outcome === 'accepted') {
        console.log('User accepted the 01-offline prompt.');
      } else {
        console.log('User dismissed the 01-offline prompt.');
      }
      deferredPrompt = null;
    });
  });
});
