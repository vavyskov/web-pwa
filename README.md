# WEB PWA

Progressive web application

Vlastnosti:
- funguje i v off-line režimu

Instalace aplikace z prohlížeče:
- desktop:
  - nefunguje v anonymním okně
  - nefunguje s "běžnou" cestou (vyžaduje "server")
  - Firefox 85+ (riziko "supercookies") vyžaduje doinstalovat plugin "Progressive Web Apps for Firefox"
- mobil:
  - Safari: Share -> Add to Home Screen

Odinstalace aplikace z prohlížeče:
- Chrome: 
  - chrome://apps
- Edge: 
  - edge://apps
  - Windows
    - Hlavní nabídka -> Aplikace (kontextová nabídka)
    - Hlavní nabídka -> Nastavení -> Aplikace

Poznámky:
- PWA aplikace je nakonfigurována tak, že na webu funguje v podadresáři "01-offline" (lokální server např. `npx http-server` spustit v kořenu projektu a přejít na adresu `http://127.0.0.1:8080/01-offline/`)
- všechny externí odkazy otevírat do nového okna prohlížeče (`target="_blank"`)

Kontrola:
- Google Chrome:
  - DevTools, **Lighthouse** (spouštět v **anonymním okně** nebo vypnout všechna rozšíření)
    - [Node.js](https://nodejs.org/en/)
    - `npx http-server`
    - `npx http-server -c31536000`
  - Rozšíření:
    - **Web Vitals**

ToDo:
- tlačítko "Odinstalovat aplikaci"
- tlačítko "Aktualizovat registraci Service Worker"
- tlačítko "Zrušit registraci Service Worker"
- informovat uživatele o nemožnosti instalace (Firefox, anonymní režim atd.)

Odkazy:
- [Progresivní webové aplikace](https://www.vzhurudolu.cz/prirucka/pwa)
- [Making a simple PWA under 5 minutes](https://www.geeksforgeeks.org/making-a-simple-pwa-under-5-minutes/)
- [**Learn PWA**](https://web.dev/learn/pwa/)
- [Add a web app manifest](https://web.dev/add-manifest/)
- [Web App Manifest generator](https://www.seochecker.it/web-app-manifest-generator)
- [Příklad notifikace](https://github.com/mdn/pwa-examples/tree/master/js13kpwa)
- [Using Service Workers](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers)
- [**Vite PWA**](https://vite-pwa-org.netlify.app/)
- [Workbox](https://developers.google.com/web/tools/workbox/)
- [**Debug Progressive Web Apps**](https://developer.chrome.com/docs/devtools/progressive-web-apps/)

Tipy:
- [Responsive web design basics](https://web.dev/responsive-web-design-basics/)
  - [Optimize Cumulative Layout Shift](https://web.dev/optimize-cls/)
  - [Testing Media Features for pointer and hover](https://glitch.com/edit/#!/media-query-pointer)
  - [Serve responsive images](https://web.dev/serve-responsive-images/)

Podpora:
- [Web Notifications](https://caniuse.com/notifications)

Nástroje:
- [Compressor.io](https://compressor.io/) (zdarma 50/den)
- [squoosh.app](https://squoosh.app/)

Zdroje:
- [pixabay.com](https://pixabay.com/service/license/) (fotky, ilustrace, vektory, video, hudba)
- [unsplash.com](https://unsplash.com/license) (fotky)
